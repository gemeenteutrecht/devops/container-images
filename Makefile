VERSION=$(shell git rev-parse HEAD)

login: 
	docker login registry.gitlab.com

build: login
	docker build -t tmptag -f images/$(DOCKERFILE) .

release: build
	docker tag tmptag registry.gitlab.com/gemeenteutrecht/devops/container-images/$(IMAGENAME):$(VERSION)
	docker tag tmptag registry.gitlab.com/gemeenteutrecht/devops/container-images/$(IMAGENAME):latest
	docker push registry.gitlab.com/gemeenteutrecht/devops/container-images/$(IMAGENAME):$(VERSION)
	docker push registry.gitlab.com/gemeenteutrecht/devops/container-images/$(IMAGENAME):latest