# Dockerfiles

A collection of Dockerfiles to build various images. All images are pushed in this [project's registry](https://gitlab.com/gemeenteutrecht/devops/container-images/container_registry). 

## Contributing
### Update an existing image
Simply create a Merge-Request with your changes in the concerning Dockerfile, e.g. in `images/Dockerfile.tooling`. Once your MR is approved and merged, the pipeline will automaticaly build and push your image in this [project's registry](https://gitlab.com/gemeenteutrecht/devops/container-images/container_registry).

### Release a new image
Add your new Dockerfile in the `images` directory. In the `.gitlab-ci.yml`, copy the following job:

```
release-containertools:
  stage: release
  extends: .release-generic
  variables:
    DOCKERFILENAME: Dockerfile.containertools
    IMAGENAME: containertools
```

Make sure to:
* rename the job (`release-containertools`)
* set the value of `DOCKERFILENAME` to the name of your new Dockerfile
* set the value of `IMAGENAME` to the container image name you'd like

### Note:
We are using custom container images that extend `alpine:3.18` base image to ensure compatibility with GitLab runners and provide possibility to extend with needed tooling.