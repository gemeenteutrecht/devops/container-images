#!/usr/bin/env python3
#
# 25-07-2024, Tony Scheffer
#
# 0.2: added json logging
#      added exception handling for HTTP calls
# 0.1: first version
#
""" this program will scrape dockerhub and retrieve the limits """

import time
import json
import requests
from prometheus_client import start_http_server, Gauge

dockerhub_limit = Gauge('dockerhub_limit', \
                        'percentage left of API calls to Dockerhub from \
                         this IP', labelnames=["limit", "ip"])

def get_token():
    """ returns a token to authenticate with dockerhub """
    url = "https://auth.docker.io/token?service=registry.docker.io&scope=\
           repository:ratelimitpreview/test:pull"
    try:
        r = requests.get(url, timeout=5)
    except requests.exceptions.Timeout as e:
        log('error', 'connection timeout error')
        log('error', str(e))
        return False
    except requests.exceptions.RequestException as e:
        log('error', 'request exception')
        log('error', str(e))
        return False
    cont = r.json()
    token = cont['token']
    return token

def get_limits():
    """ this will scrape dockerhub and get the values we need """
    token = get_token()
    if not token:
        log('error', 'could not retrieve token')
        return False, False, False
    url = 'https://registry-1.docker.io/v2/ratelimitpreview/test/manifests/latest'
    headers = {"Authorization": f"Bearer { token }"}
    # if you want to trigger the API count, to reproduce, replace below line 'head' with 'get'
    # then it will start to count as an API call
    try:
        r = requests.head(url, headers=headers, timeout=5)
    except requests.exceptions.Timeout as e:
        log('error', 'connection timeout error')
        log('error', str(e))
        return False
    except requests.exceptions.RequestException as e:
        log('error', 'request exception')
        log('error', str(e))
        return False, False, False
    if not 'ratelimit-limit' in r.headers:
        log('warning', 'could not retrieve the ratelimit from the headers')
        return False, False, False
    ratelimit_limit = r.headers['ratelimit-limit'].split(';')[0]
    ratelimit_remaining = r.headers['ratelimit-remaining'].split(';')[0]
    ip_number = r.headers['docker-ratelimit-source']
    return ratelimit_limit, ratelimit_remaining, ip_number


def get_percentage():
    """ return the percentage of API calls left """
    (ratelimit_limit, ratelimit_remaining, ip_number) = get_limits()
    if not ratelimit_limit:
        return False, False, False
    percent_left = int(ratelimit_remaining)/int(ratelimit_limit)*100
    return percent_left, ratelimit_limit, ip_number


def log(level: str, message: str) -> str:
    now = time.time_ns()
    json_object = json.dumps({'ts': int(now), 'level': level, 'message': message})
    print(json_object)



if __name__ == '__main__':
    log('info', 'starting HTTP server')
    start_http_server(8000, addr="0.0.0.0")
    log('info', 'starting program loop to retrieve current values')
    while True:
        log('debug', 'start loop, getting percentage')
        (pl, rl, ip) = get_percentage()
        if not pl:
            log('warning', 'no percantage was retrieved, sleeping and retrying')
            time.sleep(60)
            continue
        log('debug', f"setting labels: limit={ rl } ip={ ip } value={ pl }")
        dockerhub_limit.labels(limit=rl, ip=ip).set(pl)
        log('debug', 'sleeping 60 seconds untill new loop')
        time.sleep(60)
