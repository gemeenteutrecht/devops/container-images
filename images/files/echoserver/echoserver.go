package main

import (
	"fmt"
	"net/http"
)

func handler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "Returning HTTP-200!")
}

func main() {
	http.HandleFunc("/", handler)
	fmt.Println("Starting echoserver on :8081")
	// Start the server on port 8080
	if err := http.ListenAndServe(":8081", nil); err != nil {
		fmt.Println("echoserver failed:", err)
	}
}
