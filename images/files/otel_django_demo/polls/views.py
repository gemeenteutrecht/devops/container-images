from django.http import Http404
from django.http import HttpResponse
from .models import Question
from django.template import loader
from django.shortcuts import render
from .exporter import tracer


def generate_503(request):
    with tracer.start_as_current_span("generate_503_span"):
        return HttpResponse("503 man", status=503)

def my_view(request):
    with tracer.start_as_current_span("my_view_span"):
        return HttpResponse("Hello, World!")

def index(request):
    with tracer.start_as_current_span("index_span"):
        latest_question_list = Question.objects.order_by("-pub_date")[:5]
        context = {"latest_question_list": latest_question_list}
        return render(request, "polls/index.html", context)

def detail(request, question_id):
    with tracer.start_as_current_span("detail_span"):
        try:
            question = Question.objects.get(pk=question_id)
        except Question.DoesNotExist:
            raise Http404("Question does not exist")
        return render(request, "polls/detail.html", {"question": question})

def results(request, question_id):
    with tracer.start_as_current_span("results_span"):
        response = "You're looking at the results of question %s."
        return HttpResponse(response % question_id)

def vote(request, question_id):
    with tracer.start_as_current_span("vote_span"):
        return HttpResponse("You're voting on question %s." % question_id)
