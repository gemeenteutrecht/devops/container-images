#!/bin/bash
export DJANGO_SETTINGS_MODULE=thehideout.settings
export PYTHONPATH="/code:$PYTHONPATH"
python3 manage.py runserver 0.0.0.0:8000
