#!/usr/bin/env python3
""" this program check if certain domain DNS resolving works
    and this way it keeps the entrys in the cache. this way
    its more unlikely this will happen to other pods """
import os
import sys
import json
import time
import datetime
import dns.resolver


def log(level: str, message: str) -> str:
    """ this function will log in json """
    now = time.time_ns()
    now_hr = str(datetime.datetime.now())
    json_object = json.dumps({'ts': int(now), 'date': now_hr, 'level': level, 'message': message})
    print(json_object)


def main():
    """ the main loop where we iterate over the domains """
    hosts = os.environ['DOMAINS'].split(' ')
    res = dns.resolver.Resolver()
    while True:
        for host in hosts:
            try:
                log("info", f"checking: { host }")
                for rr in res.resolve(host, "A"):
                    log("info", f"{ host } resolves to { rr.address}")
            except dns.exception.Timeout as e:
                log("error", f"could not resolve { host } due to a timeout. exception: { e }")
            except dns.resolver.NXDOMAIN as e:
                log("error", f"could not resolve { host } does not exist. exception: { e }")
        time.sleep(24)

if __name__ == '__main__':
    log("info", "starting")
    if not 'DOMAINS' in os.environ:
        log("error", "DOMAINS is not set")
        sys.exit(1)
    main()
