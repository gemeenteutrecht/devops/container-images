Info
----

this container was deployed to be able to quickly backup and restore files.
this container runs as a daemonset, so you need to find the pod that runs
on the same host where the pod runs that you wish to backup.
the containers filesystem will be mounted on: 

/host_pods

inside the restic container. under /hosts_pods are uid's which represent the pods. you can find a pods
uid by checking its 'describe' in the cluster.

you can use the 'manual' bucket, which is called 'restic-manually' or use
Velero's repo's, as shown below

to recap;

- the restic manual pod needs to run on the node where the pod runs, which you
  want to backup

- /host_pods is the path in the restic-manual pod where the PVCs are mounted
  (/hosts_pods/${id}/volumes/kubernetes.io~csi/${pvc}/mount/)

- you can find the uid by using 'kubectl describe pod'


Restic commands
---------------

by default, the container is setup to talk to storage backend which is only
used by this container. See below if you want to use the existing storage
where Velero puts its backups.

this is what you can do;

use the default 'restic-manually' bucket
----------------------------------------

# list current manual backups

$ restic snapshots list

--8k--
repository 2711c17c opened (version 2, compression level auto)
created new cache in /root/.cache/restic
ID        Time                 Host                              Tags           Paths
-------------------------------------------------------------------------------------
b4fcf304  2024-04-30 11:50:14  restic-manually-748c44459f-xp7sg  manually,test  /home
-------------------------------------------------------------------------------------
1 snapshots

# its a good habit of using tags, especially since these are manual and probably be around
# for a longer period

$ restic backup --tag manually,gitlabissue999 /home

use Velero's restic repo to restore
-----------------------------------

This is no longer supported since Velero stopped using restic and moved to Kopia

az commands
-----------

az login # use the URL and device code shown to login to az using your own browser


# show subscriptions
$ az account list

parse:

$ az account list |  jq -r '.[].name' 2> /dev/null

# displays the storage accounts found and prints their name
(other storage accounts might reside in another resource group or subscription)

$ az storage account list | jq '.[].name'

# ACCOUNT_KEY and CONNECTION_STRING can be obtained from the ui
# (perhaps/prolly also via cli?)
# via uiL storage accounts, choose storage account, choose Security + networking, Access keys.

$ az storage container list --account-name=velerotest001 \
                          --account-key=$ACCOUNT_KEY \
                          --connection-string='DefaultEndpointsProtocol=https;AccountName=velerotest001;AccountKey=4WMKVYgWfv905ya99w9uzPlN9TuQEN2CuahF0sMBky3aktZYi8Ht8TZUUX2clEplvvG/a476aHuV+AStHLrv8A==;EndpointSuffix=core.windows.net' \
                          | jq '.[].name'
# to list items in a container (blobs)
# CONTAINER_NAME could be 'velero' since the container is called that inside the storage account
$ az storage blob list --container-name $CONTAINER_NAME --account-name $STORAGE_ACCOUNT_NAME --account-key=$ACCOUNT_KEY --output table

# to list blobs in the 'velero' container in the 'velerotest001' storage account;
$ az storage blob list --container-name velero --account-name velerotest001 --account-key='$ACCOUNT_KEY' --output table

# to list blobs in manually made 'tony' container, note that the account_key is the same, since the container is in the same storage account

$ az storage blob list --container-name tony --account-name velerotest001 --account-key='$ACCOUNT_KEY' --output table

