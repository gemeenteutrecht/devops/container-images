#!/usr/bin/env python3
""" this program will set the current kernel setting for max_map_count """

import os
import sys
import time
import logging


def get_current_value():
    try:
        with open(MAX_MAP_COUNT, 'rb') as file:
            data = int(file.read().decode('UTF-8'))
    except IOError as e:
        logger.error(f"could not open { MAX_MAP_COUNT }: { e }")
        sys.exit(1)
    return data


def set_value():
    logger.info('going to try to adjust the value to 262144')
    try:
        with open(MAX_MAP_COUNT, 'wb') as file:
            file.write(b'262144')
    except IOError as e:
        logger.error(f"could not write value: { e }")
        return False
    return True


def run_loop():
    logger.info("running in a loop to keep an eye on the value an set when required.")
    while True:
        current_value = get_current_value()
        if current_value < 262144:
            # show this as a warning, since the change is done while running
            logger.warning(f'current MAX_MAP_COUNT is too low: ({ current_value })')
            result = set_value()
            if not result:
                sys.exit(1)
        time.sleep(5)


def check_value():
    current_value = get_current_value()
    logger.info(f"current value { current_value }")
    if current_value < 262144:
        # show this as info, since on initial start its expected the value differs
        logger.info(f'current MAX_MAP_COUNT is too low: ({ current_value })')
        result = set_value()
        if not result:
            sys.exit(1)


def main():
    logger.info('started')
    check_value()
    run_loop()

MAX_MAP_COUNT = '/proc/sys/vm/max_map_count'
# this should be available in the pod, since its setup to
# set this env using spec.nodeName
if 'MY_HOST_NAME' in os.environ:
    HOSTNAME = os.environ['MY_HOST_NAME']
else:
    HOSTNAME = 'localhost'

logger = logging.getLogger(__name__)
logging.basicConfig(
    format=f'%(asctime)s %(levelname)s { HOSTNAME } %(message)s',
    level=logging.INFO,
    datefmt='%Y-%m-%d %H:%M:%S')

if __name__ == '__main__':
    main()
