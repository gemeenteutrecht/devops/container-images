#!/usr/bin/env python3

__author__ = "Scott van Dijk, Tony Scheffer"
__version__ = "0.1.3"

import os
import re
import json
import smtplib
import base64
from datetime import datetime
from types import SimpleNamespace
from email.mime.text import MIMEText
import requests
import adal


def load_env():
    return SimpleNamespace(
        AAD_TENANT_ID=os.environ['AZURE_TENANT_ID'],
        AAD_APP_ID=os.environ['AZURE_CLIENT_ID'],
        AAD_APP_SECRET=os.environ['AZURE_CLIENT_SECRET'],
        EMAIL_MTA_USERNAME='vcg-jobs@utrecht.nl',
        EMAIL_MTA_PASSWORD=os.environ['EMAIL_MTA_PASSWORD'],
        EMAIL_SENDER='supportvcg@utrecht.nl',
        EMAIL_RECIPIENT=os.environ['EMAIL_RECIPIENT'],
        EMAIL_HOST='outgoing.antispam-login.net',
        EMAIL_PORT=587,
        EMAIL_USE_TLS='true',
        BACKLIST=os.environ['BLACKLIST'])

def get_graph_api_token(opts):
    auth_context = adal.AuthenticationContext(
        authority='https://login.microsoftonline.com/' + opts.AAD_TENANT_ID)
    token = auth_context.acquire_token_with_client_credentials(
        resource='https://graph.microsoft.com/',
        client_id=opts.AAD_APP_ID, client_secret=opts.AAD_APP_SECRET)
    return token.get('accessToken', '')

def get_service_principal(app_object_id, opts):
    url = 'https://graph.microsoft.com/v1.0/applications/' + app_object_id
    headers = {'Authorization': 'Bearer ' + get_graph_api_token(opts)}
    try:
        response = requests.get(url, headers=headers, timeout=4)
        response.raise_for_status()
    except Exception as e:
        print(f'error: {e}')
        return False
    return response.json()

def get_service_principals(opts):
    all_sps = []
    url = 'https://graph.microsoft.com/v1.0/applications/'
    headers = {'Authorization': 'Bearer ' + get_graph_api_token(opts)}
    while True:
        try:
            response = requests.get(url, headers=headers, timeout=4)
            response.raise_for_status()
        except Exception as e:
            print(f'error: {e}')
            return False
        data = response.json()
        all_sps.append(data)
        # if the response includes '@odata.nextLink' we need to retreive more
        # pages then the initial first one. run through the link each time found
        if '@odata.nextLink' in data:
            url = data['@odata.nextLink']
            continue
        else:
            # no more pages to fetch, break out of loop
            break
    data = response.json()
    return all_sps

def get_expiration_date(secret):
    date_string = secret.get('endDateTime', '')
    # keep only YYYY-MM-DD since hours and milliseconds are not relevant
    date_string = date_string.split("T")[0]
    date_format = '%Y-%m-%d'
    expiration_date = datetime.strptime(date_string, date_format)
    return expiration_date.timestamp()

def get_alert(app_object_id, opts):
    alerts = []
    service_principal = get_service_principal(app_object_id, opts)
    if not service_principal:
        print(f'warning: app_object_id: {app_object_id} returned no value')
        return False
    secrets = service_principal.get('passwordCredentials', ())
    for secret in secrets:
        delta = get_expiration_date(secret) - datetime.now().timestamp()
        delta_days = int(delta/60/60/24)
        secret_id = secret.get('displayName')
        service_principal_id = service_principal.get('displayName')
        # only interested in anything below 30 days
        if delta_days <= 30:
            # in some objects, the displayName is empty
            if secret_id is None:
                if secret.get('customKeyIdentifier'):
                    b64_secret_id = secret.get('customKeyIdentifier')
                    b64_secret_id_bytes = b64_secret_id.encode("ascii")
                    string_bytes = base64.b64decode(b64_secret_id_bytes)
                    secret_id = string_bytes.decode('ascii')
            # if the delta is below 0, it means its already expired
            message = False
            if delta_days <= 0:
                message = f'critical : app_id: {app_object_id} sp_id: ' \
                          f'{service_principal_id} secret: {secret_id}' \
                          f' is expired for {-delta_days} day(s)'
            else:
                if app_object_id == '6504923a-98a1-4943-aa35-3988afae2476':
                    message = f'critical : app_id: {app_object_id} sp_id: ' \
                              f'{service_principal_id} secret: {secret_id}' \
                              f' is expired for {-delta_days} day(s) THIS SCRIPT DEPENDS ON THIS SP!'

                else:
                    message = f'warning  : app_id: {app_object_id} sp_id: ' \
                              f'{service_principal_id} secret: {secret_id}' \
                              f'expires in {delta_days} day(s)'
            if message:
                alerts.append(message)
    return alerts

def report(alerts, opts):
    print(f'info     : attempting to mail results')
    message = MIMEText('\n'.join(alerts))
    message['Subject'] = 'Service principal alert'
    message['From'] = opts.EMAIL_SENDER
    message['To'] = opts.EMAIL_RECIPIENT
    try:
        mta = smtplib.SMTP(opts.EMAIL_HOST, int(opts.EMAIL_PORT))
        mta.starttls()
        mta.login(opts.EMAIL_MTA_USERNAME, opts.EMAIL_MTA_PASSWORD)
        mta.sendmail(opts.EMAIL_SENDER, opts.EMAIL_RECIPIENT, message.as_string())
        mta.quit()
    except Exception as e:
        print(f"critical : there was an issue mailing the output")
        print(e)

def main(opts):
    alertlist = []
    blacklist = re.split(' |\n|,|;', os.environ['BLACKLIST'])
    nr_of_blacklists = len(blacklist)
    print(f'info     : there are {nr_of_blacklists} items in the blacklist')
    print(f'info     : retrieving all service-principals...')
    data_list = get_service_principals(opts)
    for item in data_list:
        data = item['value']
        for app in data:
            app_object_id = app['id']
            if app_object_id == '6504923a-98a1-4943-aa35-3988afae2476':
                print(f'notice   : checking my own SP: 6504923a-98a1-4943-aa35-3988afae2476')
            if app_object_id in blacklist:
                continue
            alerts = get_alert(app_object_id, opts)
            if len(alerts) >= 1:
                alertlist.append(alerts)
            # this will print all alerts found for this specific SP (item)
            for alert in alerts:
                print(alert)
    if len(alertlist) >= 1:
        mergedlist = sum(alertlist, [])
        merged_list = filter(None, mergedlist)
        report(mergedlist, opts)
    else:
        # this will appear in the email
        mylist = ['info: no alerts found']
        report(mylist, opts)
        # this is just for stdout
        print('info     : no alerts found')


if __name__ == '__main__':
    options = load_env()
    main(options)
