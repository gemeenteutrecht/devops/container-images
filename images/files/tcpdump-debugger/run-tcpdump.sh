#!/bin/bash
myip="$(ip addr show dev eth0 | awk '/inet / { print $2 }' | awk -F / '{ print $1 }')"
tcpdump "$1" -C 100 -q -i eth0 -n -p -w /tmp/capture.pcap src $myip and not dst net 10.0.0.0/8 2>&1
